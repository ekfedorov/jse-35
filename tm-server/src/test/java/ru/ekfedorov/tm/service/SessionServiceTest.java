package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.ISessionRepository;
import ru.ekfedorov.tm.api.service.ISessionService;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.repository.SessionRepository;

import java.util.ArrayList;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertTrue(sessionService.findOneById(session1.getId()).isPresent());
        Assert.assertTrue(sessionService.findOneById(session2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Session session = new Session();
        Assert.assertNotNull(sessionService.add(session));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final Session session1 = new Session();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertTrue(sessionService.contains(sessionId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Session session1 = new Session();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertNotNull(sessionService.findOneById(sessionId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Session session = new Session();
        sessionService.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionService.findOneById(sessionId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final Session session = new Session();
        sessionService.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionService.removeOneById(sessionId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Session session = new Session();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.remove(session));
    }

}
