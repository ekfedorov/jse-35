package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.addAll(tasks);
        Assert.assertTrue(taskRepository.findOneById(task1.getId()).isPresent());
        Assert.assertTrue(taskRepository.findOneById(task2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Task task = new Task();
        Assert.assertNotNull(taskRepository.add(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        final Task task = new Task();
        final User user = new User();
        final Project project = new Project();
        final String userId = user.getId();
        final String projectId = project.getId();
        final String taskId = task.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertTrue(taskRepository.bindTaskByProjectId(userId, projectId, taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final Task task1 = new Task();
        final String taskId = task1.getId();
        taskRepository.add(task1);
        Assert.assertTrue(taskRepository.contains(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskRepository.addAll(tasks);
        Assert.assertEquals(2, taskRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByProjectIdTest() {
        final Task task = new Task();
        final User user = new User();
        final Project project = new Project();
        final String userId = user.getId();
        final String projectId = project.getId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        taskRepository.add(task);
        Assert.assertFalse(taskRepository.findAllByProjectId(userId, projectId).isEmpty());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(userId, projectId).size());

        final Task task2 = new Task();
        task2.setUserId(userId);
        task2.setProjectId(projectId);
        taskRepository.add(task2);
        Assert.assertEquals(2, taskRepository.findAllByProjectId(userId, projectId).size());

        final Task task3 = new Task();
        final User user2 = new User();
        final String user2Id = user2.getId();
        task3.setUserId(user2Id);
        task3.setProjectId(projectId);
        taskRepository.add(task3);
        Assert.assertEquals(2, taskRepository.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(user2Id, projectId).size());

        final Task task4 = new Task();
        final Project project2 = new Project();
        final String project2Id = project2.getId();
        task4.setUserId(userId);
        task4.setProjectId(project2Id);
        taskRepository.add(task4);
        Assert.assertEquals(2, taskRepository.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(userId, project2Id).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllSortByUserId() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final User user = new User();
        final String userId = user.getId();
        task1.setName("b");
        task2.setName("c");
        task3.setName("a");
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        taskRepository.addAll(tasks);
        final String sort = "NAME";
        final Sort sortType = Sort.valueOf(sort);
        final Comparator<Task> comparator = sortType.getComparator();
        final List<Task> tasks2 = new ArrayList<>(taskRepository.findAll(userId, comparator));
        Assert.assertFalse(tasks2.isEmpty());
        Assert.assertEquals(3, tasks2.size());
        Assert.assertEquals(0, tasks2.indexOf(task3));
        Assert.assertEquals(1, tasks2.indexOf(task1));
        Assert.assertEquals(2, tasks2.indexOf(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Task task1 = new Task();
        final String taskId = task1.getId();
        taskRepository.add(task1);
        Assert.assertNotNull(taskRepository.findOneById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Task task = new Task();
        taskRepository.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskRepository.findOneById(taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTestByUserId() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskRepository.findOneById(userId, taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("pr1");
        taskRepository.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskRepository.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAllByProjectIdTest() {
        final Task task = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final User user = new User();
        final Project project = new Project();
        final String userId = user.getId();
        final String projectId = project.getId();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task2.setUserId(userId);
        task2.setProjectId(projectId);
        task3.setUserId(userId);
        task3.setProjectId(projectId);
        taskRepository.add(task);
        taskRepository.add(task2);
        taskRepository.add(task3);
        Assert.assertEquals(3, taskRepository.findAllByProjectId(userId, projectId).size());
        taskRepository.removeAllByProjectId(userId, projectId);
        Assert.assertTrue(taskRepository.findAllByProjectId(userId, projectId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final Task task1 = new Task();
        taskRepository.add(task1);
        final String taskId = task1.getId();
        Assert.assertTrue(taskRepository.removeOneById(taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTestByUserId() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskRepository.removeOneById(userId, taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIndexTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final User user = new User();
        final String userId = user.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        taskRepository.add(task1);
        taskRepository.add(task2);
        taskRepository.add(task3);
        Assert.assertTrue(taskRepository.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(taskRepository.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(taskRepository.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("pr1");
        taskRepository.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskRepository.removeOneByName(userId, name));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Task task = new Task();
        taskRepository.add(task);
        Assert.assertNotNull(taskRepository.remove(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTestByUserIdAndObject() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertTrue(taskRepository.remove(userId, task));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskFromProjectIdTest() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        final String taskId = task.getId();
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertTrue(taskRepository.unbindTaskFromProjectId(userId, taskId).isPresent());
        final Task task2 = taskRepository.unbindTaskFromProjectId(userId, taskId).get();
        Assert.assertNull(task2.getProjectId());
    }

}
