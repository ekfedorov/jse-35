package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ISessionRepository;
import ru.ekfedorov.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
}
