package ru.ekfedorov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    E add(@NotNull E entity);

    void addAll(@NotNull List<E> entities);

    void clear();

    boolean contains(@NotNull String id);

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAll();

    @NotNull
    Optional<E> findOneById(@Nullable String id);

    @Nullable
    E remove(@NotNull E entity);

    @NotNull
    Optional<E> removeOneById(@Nullable String id);

}
