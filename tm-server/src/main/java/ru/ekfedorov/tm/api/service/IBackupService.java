package ru.ekfedorov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.dto.Domain;

public interface IBackupService {

    @SneakyThrows
    void load();

    @SneakyThrows
    void save();

}
