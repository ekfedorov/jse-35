package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {
}
