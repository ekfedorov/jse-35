package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.ITaskEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import java.util.List;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException{
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @NotNull
    public List<Task> findTaskAllWithComparator(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().findAll(session.getUserId(), sort);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name).orElse(null);
    }

    @Override
    @WebMethod
    public boolean removeTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().remove(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public boolean removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), id, status)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    )  throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().finishById(session.getUserId(), id)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().startById(session.getUserId(), id)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().startByName(session.getUserId(), name)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().updateById(session.getUserId(), id, name, description)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Task bindTaskByProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectTaskService().bindTaskByProject(session.getUserId(), projectId, taskId)
                .orElse(null);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectTaskService().findAllByProjectId(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public Task unbindTaskFromProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectTaskService().unbindTaskFromProject(session.getUserId(), taskId)
                .orElse(null);
    }

}
