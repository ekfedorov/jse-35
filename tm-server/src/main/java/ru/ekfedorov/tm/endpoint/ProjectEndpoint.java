package ru.ekfedorov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.endpoint.IProjectEndpoint;
import ru.ekfedorov.tm.api.service.ServiceLocator;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullSessionException;
import ru.ekfedorov.tm.model.Session;
import ru.ekfedorov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public Project addProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().add(session.getUserId(), name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public Project changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().changeStatusById(session.getUserId(), id, status)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status)
                .orElse(null);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAllWithComparator(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "sort", partName = "sort") @NotNull final String sort
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().findAll(session.getUserId(), sort);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project findProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name).orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().finishById(session.getUserId(), id)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().finishByIndex(session.getUserId(), index)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().finishByName(session.getUserId(), name)
                .orElse(null);
    }

    @Override
    @WebMethod
    public boolean removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "project", partName = "project") @NotNull Project project
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public boolean removeProjectOneById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean removeProjectOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public boolean removeProjectOneByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public boolean removeProjectByIdWithTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectTaskService().removeProjectById(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().startById(session.getUserId(), id)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().startByIndex(session.getUserId(), index)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().startByName(session.getUserId(), name)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description)
                .orElse(null);
    }

    @Override
    @Nullable
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AccessDeniedException, NullSessionException {
        serviceLocator.getSessionService().validate(session);
        if (session == null) throw new NullSessionException();
        return serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description)
                .orElse(null);
    }

}
