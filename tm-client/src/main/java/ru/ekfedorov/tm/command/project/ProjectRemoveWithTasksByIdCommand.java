package ru.ekfedorov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

public final class ProjectRemoveWithTasksByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove project with tasks by id.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "remove-project-with-tasks-by-id";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[REMOVE PROJECT WITH TASKS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!endpointLocator.getProjectEndpoint().removeProjectByIdWithTask(session, id)) {
            throw new NullProjectException();
        }
    }

}
